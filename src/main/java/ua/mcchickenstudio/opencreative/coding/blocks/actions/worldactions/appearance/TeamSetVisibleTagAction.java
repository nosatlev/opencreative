/*
 * OpenCreative+, Minecraft plugin.
 * (C) 2022-2025, McChicken Studio, mcchickenstudio@gmail.com
 *
 * OpenCreative+ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenCreative+ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package ua.mcchickenstudio.opencreative.coding.blocks.actions.worldactions.appearance;

import org.bukkit.entity.Entity;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import ua.mcchickenstudio.opencreative.coding.arguments.Arguments;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.ActionType;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.Target;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.worldactions.WorldAction;
import ua.mcchickenstudio.opencreative.coding.blocks.executors.Executor;

public final class TeamSetVisibleTagAction extends WorldAction {
    public TeamSetVisibleTagAction(Executor executor, Target target, int x, Arguments args) {
        super(executor, target, x, args);
    }

    @Override
    protected void execute(Entity entity) {
        if (!getArguments().pathExists("scoreboard") || !getArguments().pathExists("team")) {
            return;
        }
        String scoreboardName = getArguments().getValue("scoreboard","board",this);
        String teamName = getArguments().getValue("team","team",this);
        Scoreboard scoreboard = getPlanet().getTerritory().getScoreboards().get(scoreboardName.toLowerCase());
        if (scoreboard == null) {
            return;
        }
        Team team = scoreboard.getTeam(teamName);
        if (team == null) return;
        String statusString = getArguments().getValue("option","always",this);
        Team.OptionStatus optionStatus = statusString.equalsIgnoreCase("never") ? Team.OptionStatus.NEVER : statusString.equalsIgnoreCase("for-own-team") ? Team.OptionStatus.FOR_OWN_TEAM : statusString.equalsIgnoreCase("for-other-teams") ? Team.OptionStatus.FOR_OTHER_TEAMS : Team.OptionStatus.ALWAYS;
        team.setOption(Team.Option.NAME_TAG_VISIBILITY,optionStatus);
    }

    @Override
    public ActionType getActionType() {
        return ActionType.WORLD_TEAM_SET_NAME_TAG_VISIBLE;
    }
}
