/*
 * OpenCreative+, Minecraft plugin.
 * (C) 2022-2025, McChicken Studio, mcchickenstudio@gmail.com
 *
 * OpenCreative+ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenCreative+ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package ua.mcchickenstudio.opencreative.coding.blocks.actions.playeractions.inventory;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ua.mcchickenstudio.opencreative.coding.arguments.Arguments;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.ActionType;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.Target;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.playeractions.PlayerAction;
import ua.mcchickenstudio.opencreative.coding.blocks.executors.Executor;

public final class SetMenuItemAction extends PlayerAction {
    public SetMenuItemAction(Executor executor, Target target, int x, Arguments args) {
        super(executor, target, x, args);
    }

    @Override
    public void executePlayer(Player player) {
        if (!(player.getOpenInventory().getTopInventory().getHolder() instanceof CustomMenu)) {
            /*
             * This check prevents from modifying server
             * menus and OpenCreative+ menus too.
             */
            return;
        }
        ItemStack item = getArguments().getValue("item",new ItemStack(Material.AIR),this);
        for (Double slot : getArguments().getNumbersList("slots",this)) {
            if (slot > player.getOpenInventory().getTopInventory().getSize()) {
                slot = player.getOpenInventory().getTopInventory().getSize() + 0.0d;
            } else if (slot < 1) {
                slot = 1.0d;
            }
            player.getOpenInventory().getTopInventory().setItem(slot.intValue()-1,item);
        }
    }

    @Override
    public ActionType getActionType() {
        return ActionType.PLAYER_SET_INVENTORY_VIEW_ITEM;
    }
}
