/*
 * OpenCreative+, Minecraft plugin.
 * (C) 2022-2025, McChicken Studio, mcchickenstudio@gmail.com
 *
 * OpenCreative+ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenCreative+ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package ua.mcchickenstudio.opencreative.coding.blocks.actions.worldactions.blocks;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import ua.mcchickenstudio.opencreative.OpenCreative;
import ua.mcchickenstudio.opencreative.coding.arguments.Arguments;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.ActionType;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.Target;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.worldactions.WorldAction;
import ua.mcchickenstudio.opencreative.coding.blocks.executors.Executor;

import static ua.mcchickenstudio.opencreative.utils.BlockUtils.isOutOfBorders;

public final class CopyBlocksAction extends WorldAction {
    public CopyBlocksAction(Executor executor, Target target, int x, Arguments args) {
        super(executor, target, x, args);
    }

    @Override
    protected void execute(Entity entity) {
        if (!getArguments().pathExists("first") || !getArguments().pathExists("second") || !getArguments().pathExists("from") ||  !getArguments().pathExists("where")) {
            return;
        }
        Location first = getArguments().getValue("first",getWorld().getSpawnLocation(),this);
        Location second = getArguments().getValue("second",getWorld().getSpawnLocation(),this);
        Location from = getArguments().getValue("from",getWorld().getSpawnLocation(),this);
        Location where = getArguments().getValue("where",getWorld().getSpawnLocation(),this);

        int minX = Math.min(first.getBlockX(),second.getBlockX());
        int minY = Math.min(first.getBlockY(),second.getBlockY());
        int minZ = Math.min(first.getBlockZ(),second.getBlockZ());

        int maxX = Math.max(first.getBlockX(),second.getBlockX());
        int maxY = Math.max(first.getBlockY(),second.getBlockY());
        int maxZ = Math.max(first.getBlockZ(),second.getBlockZ());

        BukkitRunnable runnable = new BukkitRunnable() {
            @Override
            public void run() {
                getPlanet().getLimits().setLastModifiedBlocksAmount(0);
            }
        };
        getPlanet().getTerritory().addBukkitRunnable(runnable);

        /*
         * Example
         * FIRST: 2,7,19
         * SECOND: 1,6,18
         * FROM: 0,6,17
         * WHERE: -2,7,21
         *
         * Result
         * FIRST: -2,7,22
         * SECOND: -1,8,23
         *
         * x = x(old) + (WHERE - FROM)
         */

        // 0,1,-13
        // -76 63 -86
        Vector whereFromSubtraction = where.subtract(from).toVector();
        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                for (int z = minZ; z <= maxZ; z++) {
                    if (getPlanet().getLimits().getLastModifiedBlocksAmount() > getPlanet().getLimits().getModifyingBlocksLimit()) {
                        runnable.runTaskLater(OpenCreative.getPlugin(),20L);
                        getPlanet().getTerritory().removeBukkitRunnable(runnable);
                        return;
                    }
                    getPlanet().getLimits().setLastModifiedBlocksAmount(getPlanet().getLimits().getLastModifiedBlocksAmount()+1);
                    Location oldLocation = new Location(getWorld(),x,y,z);
                    Location newLocation = oldLocation.clone().add(whereFromSubtraction);
                    if (isOutOfBorders(oldLocation) || isOutOfBorders(newLocation)) {
                        continue;
                    }
                    Block newBlock = newLocation.getBlock();
                    newBlock.setType(oldLocation.getBlock().getType(),false);
                    newBlock.setBlockData(oldLocation.getBlock().getBlockData(),false);
                }
            }
        }
        runnable.runTaskLater(OpenCreative.getPlugin(),20L);
        getPlanet().getTerritory().removeBukkitRunnable(runnable);
    }

    @Override
    public ActionType getActionType() {
        return ActionType.WORLD_COPY_BLOCKS;
    }
}
