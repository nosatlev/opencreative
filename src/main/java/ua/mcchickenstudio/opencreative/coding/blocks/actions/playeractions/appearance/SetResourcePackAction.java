/*
 * OpenCreative+, Minecraft plugin.
 * (C) 2022-2025, McChicken Studio, mcchickenstudio@gmail.com
 *
 * OpenCreative+ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenCreative+ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package ua.mcchickenstudio.opencreative.coding.blocks.actions.playeractions.appearance;

import ua.mcchickenstudio.opencreative.OpenCreative;
import ua.mcchickenstudio.opencreative.coding.arguments.Arguments;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.Target;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.ActionType;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.playeractions.PlayerAction;
import ua.mcchickenstudio.opencreative.coding.blocks.executors.Executor;
import org.bukkit.entity.Player;

import java.util.Set;

public final class SetResourcePackAction extends PlayerAction {
    public SetResourcePackAction(Executor executor, Target target, int x, Arguments args) {
        super(executor, target, x, args);
    }

    @Override
    public void executePlayer(Player player) {
        String url = getArguments().getValue("url","",this);
        if (url.isEmpty()) return;
        /*
         * We check url, because some world owners
         * can use IP logger when player downloads
         * a resource pack from owner's site.
         */
        String checkUrl = url.toLowerCase().replaceAll("^https?://(www.)?", "");
        Set<String> allowedLinks = OpenCreative.getSettings().getAllowedResourcePackLinks();
        for (String allowed : allowedLinks) {
            if (checkUrl.startsWith(allowed)) {
                player.setResourcePack(url);
                return;
            }
        }
        throw new RuntimeException("The requested url " + url + " is not trusted by server.");

    }

    @Override
    public ActionType getActionType() {
        return ActionType.PLAYER_SET_RESOURCE_PACK;
    }
}
