/*
 * OpenCreative+, Minecraft plugin.
 * (C) 2022-2025, McChicken Studio, mcchickenstudio@gmail.com
 *
 * OpenCreative+ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenCreative+ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package ua.mcchickenstudio.opencreative.coding.blocks.actions.repeatactions;
import ua.mcchickenstudio.opencreative.OpenCreative;
import ua.mcchickenstudio.opencreative.coding.arguments.Arguments;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.Action;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.ActionCategory;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.MultiAction;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.Target;
import ua.mcchickenstudio.opencreative.coding.blocks.executors.Executor;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public abstract class RepeatAction extends MultiAction {

    private final int callsLimit = 200;
    private int calls = 0;

    public RepeatAction(Executor executor, Target target, int x, Arguments args, List<Action> actions) {
        super(executor, target, x, args, actions);
    }

    @Override
    public void executeActions() {
        //super.executeActions();
        //increaseCalls();
    }

    public void increaseCalls() {
        calls++;
        if (calls > callsLimit) {
            throw new RuntimeException("Out of repeats");
        }
        BukkitRunnable runnable = new BukkitRunnable() {
            @Override
            public void run() {
                calls--;
            }
        };
        getPlanet().getTerritory().addBukkitRunnable(runnable);
        runnable.runTaskLater(OpenCreative.getPlugin(),20L);
    }

    @Override
    public ActionCategory getActionCategory() {
        return ActionCategory.REPEAT_ACTION;
    }
}
