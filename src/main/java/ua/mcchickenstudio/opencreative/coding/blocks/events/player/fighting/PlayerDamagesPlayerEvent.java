/*
 * OpenCreative+, Minecraft plugin.
 * (C) 2022-2025, McChicken Studio, mcchickenstudio@gmail.com
 *
 * OpenCreative+ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenCreative+ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package ua.mcchickenstudio.opencreative.coding.blocks.events.player.fighting;

import ua.mcchickenstudio.opencreative.coding.blocks.events.WorldEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class PlayerDamagesPlayerEvent extends WorldEvent implements KillerVictimEvent {

    private final EntityDamageByEntityEvent event;
    private final Player damager;
    private final Player victim;
    private final double damage;

    public PlayerDamagesPlayerEvent(Player damager, Player victim, EntityDamageByEntityEvent event) {
        super(damager);
        this.event = event;
        this.damager = damager;
        this.victim = victim;
        damage = event.getDamage();
    }

    public double getDamage() {
        return damage;
    }

    @Override
    public Player getKiller() {
        return damager;
    }

    @Override
    public Player getVictim() {
        return victim;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        event.setCancelled(cancelled);
    }
}
