/*
 * OpenCreative+, Minecraft plugin.
 * (C) 2022-2025, McChicken Studio, mcchickenstudio@gmail.com
 *
 * OpenCreative+ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenCreative+ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package ua.mcchickenstudio.opencreative.coding.arguments;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.minimessage.MiniMessage;
import org.bukkit.util.Vector;
import ua.mcchickenstudio.opencreative.coding.blocks.actions.Action;
import ua.mcchickenstudio.opencreative.coding.blocks.events.EventValues;
import ua.mcchickenstudio.opencreative.coding.blocks.executors.Executor;
import ua.mcchickenstudio.opencreative.coding.variables.EventValueLink;
import ua.mcchickenstudio.opencreative.coding.variables.ValueType;
import ua.mcchickenstudio.opencreative.coding.variables.VariableLink;
import ua.mcchickenstudio.opencreative.planets.Planet;
import ua.mcchickenstudio.opencreative.utils.BlockUtils;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static ua.mcchickenstudio.opencreative.utils.ErrorUtils.*;

public class Arguments {

    private final Planet planet;
    private final Executor executor;
    private final List<Argument> argumentList = new ArrayList<>();

    private final static Pattern INT_PATTERN = Pattern.compile("^-?[0-9]*$");
    private final static Pattern FLOAT_PATTERN = Pattern.compile("^-?[0-9]*\\.?[0-9]+$");

    public Arguments(Planet planet, Executor executor) {
        this.planet = planet;
        this.executor = executor;
    }

    public final void load(ConfigurationSection section) {
        for (String path : section.getKeys(false)) {
            Argument arg = loadArgument(section,path);
            if (arg != null) argumentList.add(arg);
        }
    }

    private Argument loadArgument(ConfigurationSection section, String name) {
        String configType = section.getString(name+".type");
        Object configValue = section.get(name+".value");
        if (configType == null || configType.isEmpty() || configValue == null) {
            return null;
        }
        ValueType type = ValueType.parseString(configType.toUpperCase());
        Object value = parseValue(section,name,type,configValue);
        return new Argument(planet,type,name,value);
    }

    private Object parseValue(ConfigurationSection section, String name, ValueType type, Object configValue) {
        String stringValue = configValue.toString();
        ConfigurationSection listSection = section.getConfigurationSection(name + ".value");
        switch (type) {
            case LIST:
                List<Argument> arguments = new ArrayList<>();
                if (listSection == null) {
                    return arguments;
                }
                for (String key : listSection.getKeys(false)) {
                    Argument argument = loadArgument(listSection, key);
                    if (argument != null) arguments.add(argument);
                }
                return arguments;
            case LOCATION:
                double x,y,z;
                float yaw,pitch;
                if (listSection == null) {
                    return planet.getTerritory().getWorld().getSpawnLocation();
                }
                x = listSection.getDouble("x");
                y = listSection.getDouble("y");
                z = listSection.getDouble("z");
                yaw = (float) listSection.getDouble("yaw");
                pitch = (float) listSection.getDouble("pitch");
                return new Location(planet.getTerritory().getWorld(),x,y,z,yaw,pitch);
            case VECTOR:
                if (listSection == null) {
                    return new Vector(0,0,0);
                }
                x = listSection.getDouble("x");
                y = listSection.getDouble("y");
                z = listSection.getDouble("z");
                return new Vector(x,y,z);
            case COLOR:
                int r,g,b;
                if (listSection == null) {
                    return Color.WHITE;
                }
                r = listSection.getInt("red");
                g = listSection.getInt("blue");
                b = listSection.getInt("green");
                if (r >= 0 && r <= 255 && g >= 0 && g <= 255 && b >= 0 && b <= 255) {
                    return Color.fromRGB(r,g,b);
                } else {
                    return Color.WHITE;
                }
            case VARIABLE: {
                if (listSection == null) {
                    return null;
                }
                String varName = listSection.getString("name");
                String typeString = listSection.getString("type");
                VariableLink.VariableType varType = VariableLink.VariableType.getEnum(typeString);
                if (varType == null) {
                    varType = VariableLink.VariableType.GLOBAL;
                }
                return new VariableLink(varName,varType);
            }
            case EVENT_VALUE: {
                if (listSection == null) {
                    return null;
                }
                String typeString = listSection.getString("name");
                if (typeString == null) return null;
                EventValues.Variable varType;
                if (typeString.isEmpty()) return null;
                try {
                    if (typeString.startsWith("PLOT")) {
                        typeString = typeString.replace("PLOT","PLANET");
                    }
                    varType = EventValues.Variable.valueOf(typeString);
                } catch (Exception e) {
                    return null;
                }
                return new EventValueLink(varType,executor);
            }
            case NUMBER:
                if (INT_PATTERN.matcher(stringValue).matches()) {
                    return Integer.parseInt(stringValue);
                } else if (FLOAT_PATTERN.matcher(stringValue).matches()) {
                    return Float.parseFloat(stringValue);
                }
                return 0;
            case BOOLEAN:
                return Boolean.parseBoolean(stringValue);
            case PARAMETER:
                return Float.parseFloat(stringValue);
            case ITEM:
                if (listSection == null) {
                    return new ItemStack(Material.AIR);
                }
                return ItemStack.deserialize(listSection.getValues(true));
            case PARTICLE:
                if (listSection == null) {
                    return null;
                }
                String typeString = listSection.getString("type");
                if (typeString == null || typeString.isEmpty()) return null;
                try {
                    return Particle.valueOf(typeString);
                } catch (Exception e) {
                    return typeString;
                }
            default:
                return stringValue;
        }
    }

    public final boolean pathExists(String path) {
        for (Argument argument : argumentList) {
            if (argument.getPath().equals(path)) return true;
        }
        return false;
    }

    private Argument getArg(String path) {
        for (Argument argument : argumentList) {
            if (argument.getPath().equals(path)) {
                return argument;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public final <T> Map<T,T> getMap(String path, Action action) {
        Map<T,T> map = new HashMap<>();
        Argument arg = getArg(path);
        if (arg != null) {
            try {
                if (arg.getType() == ValueType.VARIABLE) {
                    return (Map<T,T>) arg.getValue(action);
                }
            } catch(ClassCastException e) {
                return map;
            }
        }
        sendCodingDebugVariable(planet,path,map);
        return map;
    }

    @SuppressWarnings("unchecked")
    public final <T> List<T> getList(String path, Action action) {
        List<T> list = new ArrayList<>();
        Argument arg = getArg(path);
        if (arg != null) {
            try {
                if (arg.getType() == ValueType.VARIABLE) {
                    return (List<T>) arg.getValue(action);
                } else if (arg.isList()) {
                    List<Argument> args = (List<Argument>) arg.getValue(action);
                    for (Argument argument : args) {
                        list.add((T) (argument.getValue(action)));
                    }
                }
            } catch(ClassCastException e) {
                return list;
            }
        }
        sendCodingDebugVariable(planet,path,list);
        return list;
    }

    @SuppressWarnings("unchecked")
    public final List<VariableLink> getVarLinksList(String path, Action action) {
        List<VariableLink> list = new ArrayList<>();
        Argument arg = getArg(path);
        if (arg != null && arg.isList()) {
            try {
                List<Argument> args = (List<Argument>) arg.getValue(action);
                for (Argument argument : args) {
                    if (argument.value instanceof VariableLink link) {
                        list.add(link);
                    }
                }
            } catch (ClassCastException e) {
                return list;
            }
        }
        sendCodingDebugVariable(planet,path,list);
        return list;
    }

    @SuppressWarnings("unchecked")
    public final List<String> getTextList(String path, Action action) {
        List<String> list = new ArrayList<>();
        Argument arg = getArg(path);
        if (arg != null && arg.isList()) {
            try {
                List<Argument> args = (List<Argument>) arg.getValue(action);
                for (Argument textArg : args) {
                    Object textObject = textArg.getValue(action);
                    String textString = textObject.toString();
                    if (textObject instanceof ItemStack item) {
                        if (item.hasItemMeta() && item.getItemMeta() != null) {
                            textString = item.getItemMeta().getDisplayName();
                        } else {
                            textString = item.getType().name();
                        }
                    }
                    list.add(Argument.parseEntity(textString,action.getHandler().getMainActionHandler(),action));
                }
            } catch (ClassCastException e) {
                return list;
            }
        }
        sendCodingDebugVariable(planet,path,list);
        return list;
    }

    @SuppressWarnings("unchecked")
    public final List<Double> getNumbersList(String path, Action action) {
        List<Double> list = new ArrayList<>();
        Argument arg = getArg(path);
        if (arg != null && arg.isList()) {
            try {
                List<Argument> args = (List<Argument>) arg.getValue(action);
                for (Argument numberArg : args) {
                    Object object = numberArg.getValue(action);
                    list.add(parseObject(object,0.0d));
                }
            } catch (ClassCastException e) {
                return list;
            }
        }
        sendCodingDebugVariable(planet,path,list);
        return list;
    }

    @SuppressWarnings("unchecked")
    public final List<ItemStack> getItemList(String path, Action action) {
        List<ItemStack> list = new ArrayList<>();
        Argument arg = getArg(path);
        if (arg != null && arg.isList()) {
            try {
                List<Argument> args = (List<Argument>) arg.getValue(action);
                for (Argument itemArg : args) {
                    list.add((ItemStack) itemArg.getValue(action));
                }
            } catch (ClassCastException e) {
                return list;
            }
        }
        sendCodingDebugVariable(planet,path,list);
        return list;
    }

    @SuppressWarnings("unchecked")
    public final List<Location> getLocationList(String path, Action action) {
        List<Location> list = new ArrayList<>();
        Argument arg = getArg(path);
        if (arg != null && arg.isList()) {
            try {
                List<Argument> args = (List<Argument>) arg.getValue(action);
                for (Argument itemArg : args) {
                    if (itemArg.getValue(action) instanceof Location loc) {
                        loc.setWorld(planet.getTerritory().getWorld());
                        if (!BlockUtils.isOutOfBorders(loc)) {
                            list.add(loc);
                        }
                    }
                }
            } catch (ClassCastException e) {
                return list;
            }
        }
        sendCodingDebugVariable(planet,path,list);
        return list;
    }

    public VariableLink getVariableLink(String path, Action action) {
        Argument arg = getArg(path);
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
            return null;
        }
        if (arg.value instanceof VariableLink link) {
            sendCodingDebugVariable(planet,path,link);
            return link;
        }
        sendCodingDebugNotFoundVariable(planet,path);
        return null;
    }

    public Material getValue(String path, Material defaultValue, Action action) {
        Argument arg = getArg(path);
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
            return defaultValue;
        }
        if (arg.getValue(action) instanceof ItemStack item) {
            sendCodingDebugVariable(planet,path,item.getType());
            return item.getType();
        }
        if (arg.getValue(action) instanceof Block block) {
            sendCodingDebugVariable(planet,path,block.getType());
            return block.getType();
        }
        if (arg.getValue(action) instanceof Location location) {
            sendCodingDebugVariable(planet,path,location.getBlock().getType());
            return location.getBlock().getType();
        }
        sendCodingDebugNotFoundVariable(planet,path);
        return defaultValue;
    }

    public ItemStack getValue(String path, ItemStack defaultValue, Action action) {
        Argument arg = getArg(path);
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
            return defaultValue;
        }
        if (arg.getValue(action) instanceof ItemStack) {
            sendCodingDebugVariable(planet,path,arg.getValue(action));
            return (ItemStack) arg.getValue(action);
        }
        sendCodingDebugNotFoundVariable(planet,path);
        return defaultValue;
    }

    public boolean getValue(String path, boolean defaultValue, Action action) {
        Argument arg = getArg(path);
        boolean value = defaultValue;
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
        } else if (arg.getValue(action) instanceof Boolean) {
            value = (boolean) arg.getValue(action);
            sendCodingDebugVariable(planet,path,value);
        } else if (arg.getValue(action) instanceof Integer) {
            value = (getValue(path,(defaultValue ? 2 : 1), action) > 1);
            sendCodingDebugVariable(planet,path,value);
        } else if (arg.getValue(action) instanceof Float) {
            value = (getValue(path,(defaultValue ? 2f : 1f), action) > 1f);
            sendCodingDebugVariable(planet,path,value);
        } else if (arg.getValue(action) instanceof Double) {
            value = (getValue(path,(defaultValue ? 2d : 1d), action) > 1d);
            sendCodingDebugVariable(planet,path,value);
        }
        return value;
    }

    public Object getValue(String path, Action action) {
        Argument arg = getArg(path);
        Object value = "";
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
        } else {
            value = arg.getValue(action);
            sendCodingDebugVariable(planet,path,value);
        }
        return value;
    }

    public byte getValue(String path, byte defaultValue, Action action) {
        Argument arg = getArg(path);
        byte value = defaultValue;
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
        } else {
            value = parseObject(arg.getValue(action),defaultValue,action);
            sendCodingDebugVariable(planet,path,value);
        }
        return value;
    }

    public int getValue(String path, int defaultValue, Action action) {
        Argument arg = getArg(path);
        int value = defaultValue;
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet, path);
        } else if (arg.getValue(action) instanceof Long l) {
            value = l.intValue();
            sendCodingDebugVariable(planet,path,value);
        } else if (arg.getValue(action) instanceof Integer) {
            value = (int) arg.getValue(action);
            sendCodingDebugVariable(planet,path,value);
        } else if (arg.getValue(action) instanceof Float) {
            value = Math.round((float) arg.getValue(action));
            sendCodingDebugVariable(planet,path,value);
        } else if (arg.getValue(action) instanceof Double) {
            value = (int) Math.round((Double) arg.getValue(action));
            sendCodingDebugVariable(planet,path,value);
        }
        return value;
    }

    public Color getValue(String path, Color defaultValue, Action action) {
        Argument arg = getArg(path);
        Color value = defaultValue;
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
        } else if (arg.getValue(action) instanceof Color color){
            value = color;
            sendCodingDebugVariable(planet,path,color);
        }
        return value;
    }

    public float getValue(String path, float defaultValue, Action action) {
        Argument arg = getArg(path);
        float value = defaultValue;
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
        } else {
            value = parseObject(arg.getValue(action),defaultValue);
            sendCodingDebugVariable(planet,path,value);
        }
        return value;
    }

    public double getValue(String path, double defaultValue, Action action) {
        Argument arg = getArg(path);
        double value = defaultValue;
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet, path);
        } else {
            value = parseObject(arg.getValue(action),defaultValue);
            sendCodingDebugVariable(planet,path,value);
        }
        return value;
    }

    public String getValue(String path, String defaultValue, Action action) {
        Argument arg = getArg(path);
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
            return defaultValue;
        }
        sendCodingDebugVariable(planet,path,arg.getValue(action));
        return arg.getValue(action).toString();
    }

    public Component getValue(String path, Component defaultValue, Action action) {
        Argument arg = getArg(path);
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
            return defaultValue;
        }
        sendCodingDebugVariable(planet,path,arg.getValue(action));
        String text = arg.getValue(action).toString();
        try {
            if (text.contains("§")) {
                return Component.text(text);
            } else {
                Component miniMessage = MiniMessage.miniMessage().deserialize(text);
                ClickEvent clickEvent = miniMessage.clickEvent();
                if (clickEvent != null && clickEvent.action() == ClickEvent.Action.RUN_COMMAND) {
                    miniMessage = miniMessage.clickEvent(null);
                }
                return miniMessage;
            }
        } catch (Exception ignored) {}
        return Component.text(text);
    }

    public Particle getValue(String path, Particle defaultValue, Action action) {
        Argument arg = getArg(path);
        if (arg != null && arg.getValue(action) instanceof Particle particle) {
            sendCodingDebugVariable(planet,path,arg.getValue(action));
            return particle;
        }
        sendCodingDebugNotFoundVariable(planet,path);
        return defaultValue;
    }

    public char getValue(String path, char defaultValue, Action action) {
        Argument arg = getArg(path);
        if (arg != null && arg.getValue(action) != null) {
            String value = arg.getValue(action).toString();
            if (value != null && !value.isEmpty()) {
                sendCodingDebugVariable(planet,path,value.charAt(0));
                return value.charAt(0);
            }
        }
        sendCodingDebugNotFoundVariable(planet,path);
        return defaultValue;
    }

    public Location getValue(String path, Location defaultValue, Action action) {
        Argument arg = getArg(path);
        Location locationValue = defaultValue;
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
        } else if (arg.getValue(action) instanceof Location) {
            locationValue = (Location) arg.getValue(action);
            sendCodingDebugVariable(planet,path,locationValue.getX()+" "+locationValue.getY()+" "+locationValue.getZ()+" "+locationValue.getYaw()+" "+locationValue.getPitch());
        }
        locationValue.setWorld(planet.getTerritory().getWorld());
        if (BlockUtils.isOutOfBorders(locationValue)) {
            sendCodingDebugLog(planet,"Location is out of borders! " + locationValue);
            return defaultValue;
        }
        return locationValue.clone();
    }

    public Vector getValue(String path, Vector defaultValue, Action action) {
        Argument arg = getArg(path);
        Vector vectionValue = defaultValue;
        if (arg == null) {
            sendCodingDebugNotFoundVariable(planet,path);
        } else if (arg.getValue(action) instanceof Vector) {
            vectionValue = (Vector) arg.getValue(action);
            sendCodingDebugVariable(planet,path,vectionValue.getX()+" "+vectionValue.getY()+" "+vectionValue.getZ());
        }
        return vectionValue;
    }

    private Object getVariableValue(VariableLink link, Action action) {
        return planet.getVariables().getVariableValue(link,action);
    }

    public float parseObject(Object object, float defaultValue) {
        float value = defaultValue;
        if (object instanceof Integer i) {
            value = i.floatValue();
        } else if (object instanceof Float) {
            value = (float) object;
        } else if (object instanceof Double d) {
            value = d.floatValue();
        }
        return value;
    }

    public double parseObject(Object object, double defaultValue) {
        double value = defaultValue;
        if (object instanceof Integer || object instanceof Float || object instanceof Double) {
            value = Double.parseDouble(String.valueOf(object));
        }
        switch (object) {
            case null -> {
                return defaultValue;
            }
            case Float f -> value = f;
            case Long l -> value = l.doubleValue();
            case Double d -> value = d;
            case Integer i -> value = i.doubleValue();
            default -> {
            }
        }
        return value;
    }

    public byte parseObject(Object object, byte defaultValue, Action action) {
        byte value = defaultValue;
        if (object instanceof VariableLink link) {
            return parseObject(getVariableValue(link,action),defaultValue,action);
        } else if (object instanceof Integer) {
            value = (byte) object;
        } else if (object instanceof Float) {
            value = (byte) Math.round((float) object);
        } else if (object instanceof Double) {
            value = (byte) Math.round((Double) object);
        }
        return value;
    }

    public void setArgumentValue(String path, ValueType type, Object value) {
        argumentList.add(new Argument(planet,type,path,value));
    }

    public List<Argument> getArgumentList() {
        return argumentList;
    }
}
