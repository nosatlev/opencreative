#
# OpenCreative+ 5.5 - Well, it's possible.
# (C) McChicken Studio 2017-2025.
#

#
# This plugin allows players to create their worlds,
# where they can build, play and even make a code.
#

#
# Commands:
#
#  /spawn - Teleports to spawn world.
#  /locate [Player] - Displays world info, where player currently plays.
#  /join [ID] - Connects player to world with specified ID.
#  /games - Opens worlds browser
#  /cc    - Sends message to creative (global) chat for all players.
#    /cc off - Turns off creative chat for player.
#    /cc on  - Turns on creative chat for player.
#
#  /world - Opens world's settings or displays world's info
#    /world info - Displays world's info.
#    /world delete - Deletes world.
#    /world deletemobs - Removes mobs.
#  /ad - Advertises world in chat for all players.
#  /build - Changes world's mode to Build, disables PvP,
#           and gives Creative mode to world owner and builders.
#  /play  - Changes world's mode to Play, enables PvP out of spawn area,
#           and executes code made in developer mode.
#  /dev   - Connects player to developer world, where he can create
#           a code for world, that will be executed in Play mode.
#  /environment - Opens environment settings. (for world owners, world developers)
#    /env vars list - Displays list of variables.
#    /env vars size - Displays total amount of variables.
#    /env debug - Debug mode of code.
#    /env barrel - Toggles chests or barrels in dev world.
#    /env event [Material] - Changes material for event section in dev world.
#    /env action [Material] - Changes material for action section in dev world.
#    /env floor [Material] - Changes material for floor in dev world.
#  /like - Changes world's reputation by +1
#  /dislike - Changes world's reputation by -1
#
#  /give [Player] [Material] [Amount] - Gives item.               (for world owners, world developers or builders)
#  /tp [Player] (player) - Teleports to player.                   (for world owners, world developers or builders)
#  /gamemode [Gamemode] (player) - Changes player's gamemode.     (for world owners, world developers or builders)
#  /playsound [Player] [Sound] [Volume] [Pitch] - Plays a sound.  (for world owners, world developers or builders)
#  /time set [Time] - Changes world's time.                       (for world owners, world developers or builders)
#  /weather [Sun/Storm] - Changes world's weather.                (for world owners, world developers or builders)
#  /stopsound [Player] - Stops sounds.                            (for world owners, world developers or builders)
#
#  /creative - Opens menu of plugin info.
#    /creative reload - Reloads config.
#    /creative list - Displays list of loaded worlds.
#    /creative deprecated - Displays list of old worlds, which have owner,
#                           that last joined server 1 month ago.
#    /creative corrupted - Displays list of worlds, that lost settings.yml.
#    /creative load [ID] - Loads specified world.
#    /creative unload [ID] - Unloads specified world.
#    /creative maintenance - Maintenance mode settings (start, end).
#    /creative creative-chat - Creative Chat settings (on, off)
#    /creative sounds [theme] - Sets current sounds theme.
#    /creative template [folder] - Creates a new world from Minecraft
#                                  save in plugins/OpenCreative/templates
#

#
# Powered by Paper API.
# Licensed under GNU GPL v3.
# Additional features by: Vault, ProtocolLib, PlaceholderAPI, LibsDisguises.
#
# Hangar: https://hangar.papermc.io/mcchickenstudio/OpenCreative
# Source code: https://gitlab.com/eagles-creative/opencreative
# Discord: https://discord.gg/sSFCXUeq63
#

version: 5.5.0
last-world-id: 0
hide-from-tab: spectator # Hides players, that aren't in same world: spectator - Transparent nickname (requires ProtocolLib), full - Full hide from tab list, none - Not hide.

lobby:
  #
  # Lobby is a world, where player teleports on server join,
  # or on command type: /lobby, /hub, /spawn. It gives player
  # items for opening menus. To change spawn location use
  # /setworldspawn command.
  #
  world: "world"
  clear-inventory: true # Inventory will be cleared on server join, world change, should be enabled due to security reasons.

recommended-worlds: [1,2,3,4,5] # List of featured worlds in /games, type world number IDs here.

messages:
  locale: en # Supported: en, ru - Для русского языка: ru
  branding: "<white>Open<gradient:#dbdbdb:#ffd4c2>Creative</gradient><green>+" # Branding uses minimessage (https://webui.advntr.dev/)
  prefix: "&6 Worlds &8| &f"
  cc-prefix: "&6 Creative Chat &8| &7"
  cc-chat: "%cc-prefix%&7%player%&7: %message%" # You can use PAPI placeholders here
  world-chat: "&7 %player% &8» &f%message%" # and here
  version: "&f \n &fPowered by Open&7Creative&b+ &3%version%\n &f%codename% \n \n &fContibutors: &7McChicken Team\n &fTranslators: &7initzero, Nagibator6000Lol \n \n &cMcChicken Studio 2017-2025\n &c"
  critical-errors: true # Will send critical errors in console
  warnings: true # Will send warning in console
  not-found: true # Will send not found message in console

allowed-links:
  # These links are allowed to use in Request Resource Pack action.
  resource-pack:
    - "dropbox.com"
    - "drive.google.com"

commands:
  #
  # These commands will be executed on
  # some events. You can add functionality,
  # like sending message with DiscordSRV.
  #
  onLobby: # Event (%player%)
    first: # Command name
      command: "" # Command to execute, without slashes /
      delay: 0 # Delay (in ticks, 1 second = 20 ticks)
      console: true # true - Execute as console, false - as player
  onPlanetConnect: {} # %planet% %player%
  onPlanetDisconnect: {} # %planet% %player%
  onWorldChat: {} # %world% %player% %message% %formatted%
  onCreativeChat: {} # %player% %message% %formatted%

requirements:
  #
  # If player's input new text will not follow
  # these requirements, then world's setting
  # will be not changed.
  #
  world-creation:
    played-seconds: 30
  world-reputation:
    creation-seconds: 300
  world-name:
    min-length: 4
    max-length: 30
  world-description:
    min-length: 4
    max-length: 256
  world-custom-id:
    pattern: "^[a-zA-Zа-яА-Я0-9_]+$"
    min-length: 2
    max-length: 16

groups:
  #
  # Groups are made for specifying players permissions,
  # like world size, world limits, worlds amount.
  #
  default: # default - by default for any player, no permissions required
    cooldowns:
      generic-command: 5 # /play, /build, /games
      advertisement: 120 # /ad
      creative-chat: 10 # /cc
      world-chat: 2 # chat in world
    creating-world:
      limit: 2
    world:
      size: 25
      like-reward: 1
      advertisement-cost: 0
      limits:
        #
        # Limits for world, that player created.
        #
        executor-calls: 100 # limit of executor calls
        entities-amount: 50 # limit for spawned entities in world
        modifying-blocks: 5000 # limit for changing blocks with world actions per second
        redstone-changes: 100 # limit for redstone operations in world
        opening-inventories: 5 # limit for opening inventories per 5 seconds for preventing anti-exit
        variables-amount: 50 # limit for amount of all variables in world (saved, global, local)
        scoreboards-amount: 50 # limit for amount of all scoreboards
        bossbars-amount: 10 # limit for amount of all bossbars
        coding-platforms: 4 # limit for coding platforms
      per-player-limit-modifiers:
        #
        # When player joins a world or leaves from world,
        # this will increase or decrease limits values.
        #
        executor-calls: 100
        entities-amount: 50
        modifying-blocks: 0
        redstone-changes: 20
        opening-inventories: 5
        variables-amount: 50
        scoreboards-amount: 3
        bossbars-amount: 3
      build-permissions: # Permissions will be given when player types /build command
        - "headdb.open"
        - "worldedit.wand"
        # If you want to add WorldEdit permissions please turn off
        # region-restrictions in WorldEdit/config.yml to make //set working
        #- "worldedit.region.set"
      play-permissions: # Permissions will be given when player types /play command
        - "headdb.open"
      dev-permissions: # Permissions will be given when player types /dev command
        - "headdb.open"
        - "essentials.itemname"
  premium:
    permission: "opencreative.group.premium"
    cooldowns:
      generic-command: 3
      advertisement: 100
      creative-chat: 4
      world-chat: 1
    creating-world:
      limit: 4
    world:
      size: 50
      like-reward: 1
      advertisement-cost: 0
      limits:
        executor-calls: 120
        entities-amount: 200
        modifying-blocks: 7500
        redstone-changes: 200
        opening-inventories: 5
        variables-amount: 500
        scoreboards-amount: 50
        bossbars-amount: 10
        coding-platforms: 4
      per-player-limit-modifiers:
        executor-calls: 30
        entities-amount: 50
        modifying-blocks: 0
        redstone-changes: 20
        opening-inventories: 5
        variables-amount: 50
        scoreboards-amount: 10
        bossbars-amount: 10
      build-permissions:
        - "headdb.open"
        - "worldedit.wand"
        #- "worldedit.region.set"
        #- "worldedit.region.replace"
      play-permissions:
        - "headdb.open"
      dev-permissions:
        - "headdb.open"
        - "essentials.itemname"

sounds:
  #
  # This section allows you to set custom
  # sounds. If sound is not listed here,
  # build-in sound will be used instead.
  # Themes are useful for special days
  # or holidays.
  #
  # Themes can be changed with:
  # /oc sounds [theme name]
  #
  # To add sound from resource pack use
  # name: "resource_pack_name:sound.id"
  #
  # lobby, lobby-music, opencreative, reloading, reloaded, menus-next-page, menus-previous-page, menus-open-world-access,
  # menus-next-choice, menus-open-generation, menus-open-environment, menus-open-world-settings, menus-open-world-moderation,
  # menus-open-recommendations, menus-open-own-worlds-browser, menus-open-worlds-browser, menus-open-entities-browser,
  # menus-open-values-browser, menus-open-confirmation, menus-generation-change, menus-environment-change, menus-entities-browser-sort,
  # menus-worlds-browser-sort, menus-worlds-browser-category, menus-world-search, menus-generate-structures-change, world-generation,
  # world-connection, world-connected, world-liked, world-disliked, welcome-to-new-world, world-mode-build, world-mode-dev,
  # world-now-builder, world-now-developer, world-now-developer-guest, world-kicked, world-banned, player-cancel, player-fail,
  # player-error, player-teleport, player-respawn, world-code-error, world-code-compile-error, world-code-critical-error,
  # world-settings-flag-change, world-settings-category-set, world-settings-time-change, world-settings-autosave-on,
  # world-settings-autosave-off, world-settings-sharing-public, world-settings-sharing-private, world-settings-spawn-teleport,
  # world-settings-spawn-set, world-settings-owner-set, world-purchase, world-remove-entity, world-teleport-to-entity,
  # world-teleport-entity-to-me, dev-connected, dev-not-allowed, dev-open-chest, dev-open-barrel, dev-closed-chest,
  # dev-closed-barrel, dev-set-event, dev-set-action, dev-set-condition, dev-set-target, dev-set-method, dev-set-function,
  # dev-var-list, dev-platform-color, dev-platform-claim, dev-action-target, dev-action-with-chest, dev-condition-not,
  # dev-condition-default, dev-cycle-delay-decrease, dev-cycle-delay-increase, dev-cycle-delay-set, dev-cycle-named,
  # dev-function-named, dev-method-named, dev-variable-parameter, dev-next-parameter, dev-change-category, dev-take-value,
  # dev-value-set, dev-text-set, dev-number-set, dev-particle-set, dev-variable-set, dev-variable-change, dev-fly-speed-change, d
  # ev-potion-set, dev-location-set, dev-location-teleport, dev-location-teleport-back, dev-event-value-set, dev-vector-set,
  # dev-boolean-true, dev-boolean-false, dev-move-blocks-right, dev-move-blocks-left, dev-debug-on, dev-debug-off, maintenance-notify,
  # maintenance-count, maintenance-start, maintenance-end, world-load, world-unload, world-deletion
  #
  theme: "default"
  default:
    lobby-music:
      name: "music_disc.precipice"
      pitch: 0.1
  christmas:
    lobby-music:
      name: "music_disc.chirp"
      pitch: 1.7

items:
  #
  # This section allows you to change items.
  # If you edit with text editor you're able
  # only to set material.
  # lobby-my-worlds: "COMPASS"
  #
  # To set additional item data (enchantments, colors,
  # textures) you have to use in-game command while
  # holding item for setting in right-hand.
  # /oc items set lobby-my-worlds
  #
  # Some items can break in next Minecraft updates,
  # so if item is failed to create it will use default one.
  lobby-worlds-browser: "COMPASS"

#
# Maintenance mode unloads all worlds and
# prevents players without admin permissions
# from loading or creating worlds.
#
# /creative maintenance start 30
# /creative maintenance end
#
maintenance: false
#
# In Debug mode OpenCreative+ will send some
# information about plugin's work and state.
# Not useful, if you're not developing OC+.
#
debug: true
