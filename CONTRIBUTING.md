# Contributing Guidelines

You can support OpenCreative+ project with: code contributing, bug reports and creating forks.

![OpenCreative+ Development World](https://i.imgur.com/O9Ky4oH.png)

# Bug reports 🪲

Report exceptions, errors and bugs in [Issues site](https://gitlab.com/eagles-creative/opencreative/-/issues).

# Development 🧑‍💻

### Software

We recommend using Intellij Idea to make changes in code, but you can
use built-in Gitlab code editing tools to make minor changes.

### Code guidelines

Your Java code, as our code, should follow [guidelines by Google](https://google.github.io/styleguide/javaguide.html).
Guide in a nutshell:
1. **Class names are written in UpperCamelCase.**

✅ It's good: PlayerUtils, OwnWorldsMenu, Profile

❌ It's bad: playerUtils, ownworldsmenu, PROFILE

2. **Method names are written in lowerCamelCase.**

✅ It's good: createWorld, clearPlayer, teleportAsync 

❌ It's bad: createworld, ClearPlayer, TELEPORTASYNC 

3. **Don't write a very long code, you can separate it in methods or classes.**

4. **You should make code simple to understand.**

5. **You can add comments to make easier to understand.**

### Contributing

Contribute your new code to [development branch](https://gitlab.com/eagles-creative/opencreative/-/tree/development), and then
open a new merge request. We will analyze your code and we will decide to add it or not.



